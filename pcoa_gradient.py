#!/usr/bin/env python

import re
import sys
import traceback
from itertools import cycle

import matplotlib.pyplot as plt
import numpy as np

import pie

shapes = cycle(['o', 'D', 'v', '1', '*'])

def print_usage(e=None):
    if e:
        print >> sys.stderr, traceback.format_exc()
    print >> sys.stderr, \
        "%s: map.txt pcoa_coords.txt otu_table.txt metakey otukey output.png" \
        %(sys.argv[0])


def search_otus(taxa, otukey):
    for i, taxon in enumerate(taxa):
        if re.search(otukey, taxon):
            return i

    raise Exception("otu key %s not found"%(otukey))

def plot_heat(taxa_arr, pcoa_mtx, taxon):
    x, y = zip(*pcoa_mtx)
    return plt.scatter(x, y, c=taxa_arr, marker=shapes.next())

def title_text(s):
    return re.sub(r'[a-z]__', '', s.split("; ")[-2])

def main():
    try:
        (meta_fname, pcoa_fname, taxa_fname, 
         metakey, otukey, plot_fname) = sys.argv[1:]
    except Exception as e:
        print_usage(e)
        sys.exit(1)
        
    taxa, taxa_data = zip(*pie.txtload(taxa_fname))
    taxa_data = np.array(taxa_data)
    pcoa_data = np.loadtxt(pcoa_fname, delimiter=",", skiprows=1, usecols=(1,2))
    metadata = pie.get_metadata(meta_fname)
    idx_dict = pie.metagroup_indexes(metadata, meta_key=metakey)
    selected_otu = search_otus(taxa, otukey)

    plots = list()
    for group_key, group_idxs in idx_dict.iteritems():
        group_idxs = list(group_idxs)
        plots.append((
            plot_heat(taxa_data[selected_otu, group_idxs], 
                      pcoa_data[group_idxs],
                      taxa[selected_otu]),
            group_key
        ))

    plt.title("Abundance heatmap for %s"%(title_text(taxa[selected_otu])))
    plt.ylabel("Dimension 2")
    plt.xlabel("Dimension 1")
    collections, names = zip(*plots)
    plt.legend(collections, names, scatterpoints=1, loc="lower left", 
               ncol=3, fontsize=8)
    plt.savefig(plot_fname)

    
if __name__ == '__main__':
    ret = main()
    sys.exit(ret)
