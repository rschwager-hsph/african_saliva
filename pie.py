#!/usr/bin/env python

import re
import sys
import traceback
from collections import namedtuple, defaultdict
from itertools import groupby, izip, count
from operator import attrgetter, itemgetter

import numpy as np
import matplotlib.pyplot as plt

category20 = [ 0x393b79, 0x5254a3, 0x6b6ecf, 0x9c9ede,
               0x637939, 0x8ca252, 0xb5cf6b, 0xcedb9c,
               0x8c6d31, 0xbd9e39, 0xe7ba52, 0xe7cb94,
               0x843c39, 0xad494a, 0xd6616b, 0xe7969c,
               0x7b4173, 0xa55194, 0xce6dbd, 0xde9ed6 ]


tophylum = lambda name: ";".join(name.split(";")[:2])

def getter(key):
    def wrapped(obj):
        return re.sub(r'[\t /\\]+', '_', getattr(obj, key))

    return wrapped

def print_usage(e=None):
    if e:
        print >> sys.stderr, traceback.format_exc()
    print >> sys.stderr, "%s: phylum_table.txt genus_table.txt map.txt" %(
        sys.argv[0])


def to_rgb(hexval):
    s = hex(hexval)
    return ( int(s[2:4], base=16)/255., 
             int(s[4:6], base=16)/255., 
             int(s[6: ], base=16)/255. ) 


def determine_colors(phylum_names, genus_names):
    n_phyla = len(phylum_names)
    pcolors = map(to_rgb, category20[:n_phyla])
    groupwith = tophylum
    gcolors = list()
    for i, (_, phylum_group) in enumerate(groupby(genus_names, groupwith)):
        basecolor = pcolors[i]
        progression = count(1.1, 0.1)
        for genus in phylum_group:
            gcolors.append([ 
                min(val*brighten, 1.0)
                for val, brighten in izip(basecolor, progression)
            ])

    return pcolors, gcolors
        

def txtload(fname, convert_func=float, get_metadata=False):
    with open(fname) as f:
        meta = f.readline() # bump off metadata line
        if get_metadata:
            yield meta.strip().split('\t')
        for line in f:
            fields = line.strip().split('\t')
            yield fields[0], map(convert_func, fields[1:])


def load_data(*default):
    try:
        phylum_fname, genus_fname = default or sys.argv[1:3]
    except Exception as e:
        print_usage(e)
        sys.exit(1)

    (phyla, pdata) = zip(*txtload(phylum_fname))
    (geni, gdata) = zip(*txtload(genus_fname))

    return (phyla, np.array(pdata)), (geni, np.array(gdata))


def get_metadata(default=None):
    try:
        metadata_fname = default or sys.argv[3]
    except Exception as e:
        print_usage(e)
        sys.exit(1)
        
    metadata = txtload(metadata_fname, convert_func=str, get_metadata=True)
    Sample = namedtuple("Sample", metadata.next())
    meta = list()
    for i, row in enumerate(metadata, 1):
        try:
            meta.append(Sample._make([row[0]] + row[1]))
        except Exception as e:
            print >> sys.stderr, "error on row %i"%(i)
            print >> sys.stderr, e

    return meta


def metagroup_indexes(metadata, meta_key="cohort"):
    get = getter(meta_key)
    groups = defaultdict(list)
    for i, sample in enumerate(metadata):
        groups[get(sample)].append(i)
    return groups


def group_by_meta(metadata, taxonomy_data, meta_key="cohort"):
    idx_dict = metagroup_indexes(metadata, meta_key)
    (phyla, pdata), (geni, gdata) = iter(taxonomy_data)
    for group_key, group_idxs in idx_dict.iteritems():
        yield pdata[ : ,group_idxs], gdata[ : ,group_idxs]


def get_pie_data(metadata, taxonomy_data, meta_key="cohort"):
    (phyla, pdata), (geni, gdata) = iter(taxonomy_data)
    paired_metagroups = group_by_meta(metadata, taxonomy_data, meta_key)
    summed_pgroups, summed_ggroups = zip(*[ 
        (group_pdata.sum(axis=1), group_gdata.sum(axis=1))
        for group_pdata, group_gdata in paired_metagroups 
    ])

    return phyla, geni, summed_pgroups, summed_ggroups


def genus_sort(geni, genus_data):
    geni_and_data = zip(geni, genus_data)
    abundance = itemgetter(1)
    step1 = sorted(geni_and_data, key=abundance, reverse=True)
    step2 = sorted(step1, key=lambda row: tophylum(row[0]))
    return zip(*step2)

def plot_pie(phylum_names, genus_names, phylum_data, genus_data, 
             *args, **kwargs):
    fix, ax = plt.subplots()
    ax.axis('equal')
    width = 0.35
    
    pcolors, gcolors = determine_colors(phylum_names, genus_names)
    inside, _ =  ax.pie(phylum_data, radius=1-width, 
                        colors=pcolors, startangle=90)
    outside, _ = ax.pie(genus_data, radius=1, 
                        colors=gcolors, startangle=90)
    plt.setp(inside+outside, width=width, edgecolor="white")
    
    #ax.legend(inside[::-1], phylum_names, frameon=False)
    
    plt.savefig(*args, **kwargs)


def main():
    m = get_metadata()
    x = load_data()
    phyla, geni, pgroups, ggroups = get_pie_data(m, x)
    group_names = metagroup_indexes(m).keys()
    for phylum_data, genus_data, name in zip(pgroups, ggroups, group_names):
        geni, genus_data = genus_sort(geni, genus_data)
        plot_pie(phyla, geni, phylum_data, genus_data, name+".png")
    

if __name__ == '__main__':
    ret = main()
    sys.exit(ret)
