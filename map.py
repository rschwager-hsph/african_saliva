#!/usr/bin/env python

import re
import sys

import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits.basemap import Basemap

def setup_map():
    map_boundary = np.array([[ 22.5,  37.5],
                             [ 9.5,  23.5]])
    # fig = plt.figure()
    m = Basemap(projection='cyl',
                 llcrnrlat=map_boundary[1,0],
                 urcrnrlat=map_boundary[0,0],
                 llcrnrlon=map_boundary[1,1],
                 urcrnrlon=map_boundary[0,1],
                 resolution="f")


    m.bluemarble()
    m.drawparallels(np.arange(-90.,120.,30.))
    m.drawmeridians(np.arange(0.,420.,60.))
    return m


def parse_fname(s):
    return map(int, re.sub(r'[^0-9_]+', '', s).split("_"))

def main():
    map_of_africa = setup_map()
    lon, lat = zip(*map(parse_fname, sys.argv[1:-1]))
    x, y = map_of_africa(lat, lon)
    map_of_africa.plot(x,y,'wo')

    plt.savefig(sys.argv[-1])
    

if __name__ == '__main__':
    ret = main()
    sys.exit(ret)
