#!/usr/bin/env python

import sys
import traceback
from itertools import izip

import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec
from scipy.stats import norm
import numpy as np

import linregress
import pie

def print_usage(e=None):
    if e:
        print >> sys.stderr, traceback.format_exc()
    print >> sys.stderr, \
        "%s: map.txt, otu_table.txt plot_name.png" \
        %(sys.argv[0])



def ecdf(data_arr, cutoff):
    counts = np.sum(data_arr[:,np.newaxis] <= cutoff, axis=0)
    return counts / float(len(data_arr))

def epsilon(sample_size, alpha):
    return np.sqrt( np.log(2./alpha) / (2*sample_size) )

def ks_plot(x_domain, ecdf_range, epsilon, plt_axis):
    plt_axis.plot(x_domain, x_domain, 'k--', label="Ideal")
    plt_axis.plot(x_domain, x_domain+epsilon, 'r-')
    plt_axis.plot(x_domain, x_domain-epsilon, 'r-')
    plt_axis.plot(x_domain, ecdf_range, 'b-')
    plt_axis.set_ylabel("$\hat{F}_n(X)$")
    plt_axis.set_xlabel("$F_{model}(X)$")


def qq_plot(x, y, plt_axis, model_label):
    plt_axis.plot(x, x, 'k--', label="Ideal")
    plt_axis.plot(x, y, 'r.',  label="Model "+model_label) 
    plt.setp(plt_axis.get_xticklabels(), rotation=45, fontsize=8)
    plt.setp(plt_axis.get_yticklabels(), fontsize=8)
    plt_axis.set_ylabel("$\hat{F}^{-1}_n(X)$")
    plt_axis.set_xlabel("$F^{-1}_{model}(X)$")


def plot_readcounts(qq_axis, ks_axis, taxa_data):
    readcounts = taxa_data.sum(axis=0)
    ep = epsilon(len(readcounts), 0.05)
    m, s = readcounts.mean(), readcounts.std()
    model = norm(m, scale=s)

    perc_points = np.arange(0.05, 1, 0.05)
    qq_plot(x = np.percentile(readcounts, list(perc_points*100)),
            y = model.ppf(perc_points),
            plt_axis = qq_axis,
            model_label = "$N(%.2f, %.2f)$"%(m, s))

    mn, mx = readcounts.min(), readcounts.max()
    cdf_points = np.arange(mn, mx, (mx-mn)/20)
    ks_plot(x_domain = model.cdf(cdf_points),
            ecdf_range = ecdf(readcounts, cdf_points),
            epsilon = ep,
            plt_axis = ks_axis)
    

def plot_taxa_counts(qq_axis, ks_axis, taxa_data):
    ep = epsilon(taxa_data.shape[1], 0.05)
    perc_points = np.arange(0.05, 1, 0.05)

    filtered = iter(row[ ~np.isinf(row) ] for row in taxa_data )
    rows = iter( (row, row.mean(), row.std(), row.min(), row.max())
                 for row in filtered )

    taxon_counts, m, s, mn, mx = rows.next()
    model = norm(m, scale=s)
    qq_plot(x = np.percentile(taxon_counts, list(perc_points*100)),
            y = model.ppf(perc_points),
            plt_axis = qq_axis,
            model_label = "$N(%.2f, %.2f)$"%(m, s))

    cdf_points = np.arange(mn, mx, (mx-mn)/20)
    ks_plot(x_domain = model.cdf(cdf_points),
            ecdf_range = ecdf(taxon_counts, cdf_points),
            epsilon = ep,
            plt_axis = ks_axis)

    for taxon_counts, m, s, mn, mx in rows:
        model = norm(m, scale=s)
        perc = np.percentile(taxon_counts, list(perc_points*100))
        qq_axis.plot(perc,
                     model.ppf(perc_points), 'r.')

        cdf_points = np.arange(mn, mx, (mx-mn)/20)
        ks_axis.plot(model.cdf(cdf_points),
                     ecdf(taxon_counts, cdf_points), 'b-')

    qq_axis.plot(perc, perc, 'k--')

def main():
    try:
        meta_fname, otu_fname, plot_fname = sys.argv[1:]
    except Exception as e:
        print_usage(e)
        sys.exit(1)
    
    taxa, taxa_data = linregress.load_otu_table(otu_fname)

    grid = GridSpec(2,2)
    rc_qq_axis, rc_ks_axis = plt.subplot(grid[0,0]), plt.subplot(grid[0,1])
    taxa_qq_axis, taxa_ks_axis = plt.subplot(grid[1,0]), plt.subplot(grid[1,1])

    plot_readcounts(rc_qq_axis, rc_ks_axis, taxa_data)
    taxa_data = np.log(taxa_data)
    plot_taxa_counts(taxa_qq_axis, taxa_ks_axis, taxa_data)
    
    plt.tight_layout()
    plt.savefig(plot_fname, dpi=500)


if __name__ == '__main__':
    ret = main()
    sys.exit(ret)
