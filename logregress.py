#!/usr/bin/env python

import sys
from operator import attrgetter
from itertools import izip

import scipy.stats
import numpy as np

import pie

def print_usage(e=None):
    if e:
        print >> sys.stderr, traceback.format_exc()
    print >> sys.stderr, \
        "%s: map.txt, otu_table.txt metakey" \
        %(sys.argv[0])


def indicators(samples, key):
    values = map(key, samples)
    names = set(values)
    def _col():
        for name in names:
            yield [ int(v==name) for v in values ]

    return names, np.array(list(_col()))

def predictors(samples, attr):
    key = attrgetter(attr)
    try:
        float(key(samples[0]))
    except:
        return indicators(samples, key)
    else:
        return [attr], np.array([[ float(getattr(s, attr)) for s in samples ]])


def regress(target, design_mtx):
    Y = target
    X = design_mtx
    fish_inv = (X.T * X).I
    predictor_weights = fish_inv * X.T * Y
    predictions = design_mtx * predictor_weights
    residuals = target - predictions
    nrow, ncol = design_mtx.shape
    est_variance = (1./(nrow-ncol)) * residuals.T * residuals
    
    return fish_inv, predictor_weights, predictions, residuals, est_variance

def column(array):
    """make into a column vector"""
    return array[:,np.newaxis]

def uncolumn(col_vector):
    "make a column vector back into a 1-d array"
    return np.array(col_vector)[:,0]

def otu_iter(fname, skiprows=2):
    converter = lambda i: int(float(i))
    with open(fname) as f:
        zip(f, range(skiprows)) #skip some rows
        for line in f:
            fields = line.strip().split('\t')
            yield fields[-1]+"; "+fields[0], map(converter, fields[1:-1])


def take(indexable, idxs):
    return [indexable[idx] for idx in idxs]


def idx_filter(iterable, filter_idxs):
    for item, i_should_yield in izip(iterable, filter_idxs):
        if i_should_yield:
            yield item


def sparsity_filter(data, min_abundance=0.001, min_samp_perc=0.1):
    min_samples = min_samp_perc * data.shape[1]
    col_totals = data.sum(axis=0)
    norm = data.astype('float') / col_totals
    return (norm > min_abundance).sum(axis=1) > min_samples


def load_otu_table(fname):
    names, data = zip(*otu_iter(fname))
    data = np.array(data)
    filter_ = sparsity_filter(data)

    return idx_filter(names, filter_), data[filter_, : ]


def regress_one(count_arr, inds, readcount, name):
    _filter = count_arr > 0
    count_arr = np.log(count_arr[_filter])
    target = column(count_arr)
    if len(inds) > 1:
        inds = inds[:-1]

    design_mtx = np.matrix( np.hstack((
        np.ones(target.shape),
        column(readcount[_filter]),
        inds[:,_filter].T
    )) )
    
    try:
        fish_inv, coefs, _, _, est_var = regress(target, design_mtx)
    except np.linalg.linalg.LinAlgError as e:
        print >> sys.stderr, "Skipped because of singular matrix: %s" %name
        return None

    diag = np.array(fish_inv.diagonal())[0]
    if np.any(diag < -1):
        print >> sys.stderr, "Skipped because of negative fisher inverse: %s" \
            %name
        return None

    z_scores = uncolumn(coefs) / np.sqrt(diag)
    degrees = target.shape[0] - design_mtx.shape[1]
    p_values = scipy.stats.t(degrees).cdf(-np.abs(z_scores)) # 2-tailed t-test

    return map(np.array, (p_values, coefs))
    

def fdr_correct(p_values):
    sort_idxs = np.argsort(p_values, axis=None)
    p_values = p_values.flatten()[sort_idxs]
    scale = len(p_values) / (np.arange(len(p_values)).astype('float')+1)
    
    return p_values * scale, sort_idxs


def regress_all(taxa, taxa_data, group_names, inds):

    readcount = np.sum(taxa_data, axis=0)
    items = iter( (name, regress_one(count_arr, inds, readcount, name))
                  for count_arr, name in izip(taxa_data, taxa) )
    taxa_names, p_values, coefs = zip(*[ 
        ([name]*len(item[0]), item[0], item[1])
        for name, item in items if item
    ])

    if len(group_names) > 1:
        coef_names = [ group_names[-1], "Read Count"] + group_names[:-1]
    else:
        coef_names = [ "Intercept", "Read Count"] + group_names
    coef_names = np.array( coef_names * len(p_values) )
    taxa_names = np.array(taxa_names)
    p_values = np.array(p_values)

    sorted_qvalues, sort_idxs = fdr_correct(p_values)
    coefs = np.array(map(uncolumn, coefs))

    return izip(taxa_names.flatten()[sort_idxs],
                coef_names[sort_idxs],
                coefs.flatten()[sort_idxs],
                p_values.flatten()[sort_idxs],
                sorted_qvalues)

def main():
    try:
        meta_fname, taxa_fname, metakey = sys.argv[1:]
    except Exception as e:
        print_usage(e)
        sys.exit(1)

    metadata = pie.get_metadata(meta_fname)
    taxa, taxa_data = load_otu_table(taxa_fname)

    mask = [ not bool(s.Latitude == "N/A") for s in metadata ]
    taxa_data = taxa_data[:,np.array(mask)]
    metadata = list(idx_filter(metadata, mask))

    names, preds = predictors(metadata, metakey)

    l_fmt = "%s\t%s\t%.2f\t%f\t%f"
    s_fmt = "%s\t%s\t%.2f\t%.4e\t%.4e"
    print "\t".join(("Taxon", "Variable", "Effect size", "p-value", "q-value"))
    for row in regress_all(taxa, taxa_data, list(names), preds):
        if row[-1] > 0.0001 or row[-2] > 0.0001:
            print l_fmt % row
        else:
            print s_fmt % row

if __name__ == '__main__':
    ret = main()
    sys.exit(ret)
