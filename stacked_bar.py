#!/usr/bin/env python

import re
import traceback
import sys
from itertools import izip, cycle, groupby, count
import operator

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec

import pie
import linregress

def print_usage(e=None):
    if e:
        print >> sys.stderr, traceback.format_exc()
    print >> sys.stderr, "%s: map.txt phylum_table.txt metakey plot.png" %(
        sys.argv[0])

category10 = [
  0x1f77b4, 0xff7f0e, 0x2ca02c, 0xd62728, 0x9467bd,
  0x8c564b, 0xe377c2, 0x7f7f7f, 0xbcbd22, 0x17becf
]

tophylum = lambda name: ";".join(name.split(";")[:2])

def title_text(s):
    return re.sub(r'[a-z]__', '', s.split("; ")[-1])

def take(indexable, idxs):
    return [indexable[idx] for idx in idxs if idx < len(indexable)]

def idx_filter(iterable, filter_idxs):
    return [ item for item, drop_it in izip(iterable, filter_idxs) 
             if not drop_it]

def group_by_meta(metadata, taxonomy_row, meta_key="cohort"):
    idx_dict = pie.metagroup_indexes(metadata, meta_key)
    for group_key, group_idxs in idx_dict.iteritems():
        yield group_key, sum(take(taxonomy_row, group_idxs))


def colors_gen():
    progression = count(1.0, 0.1)
    for brighten_factor in progression:
        for color in map(pie.to_rgb, category10):
            yield [ min(val*brighten_factor, 1.0) for val in color ]


def plot_stacked_bar(metadata, taxa, taxa_data):
    grid = GridSpec(1, 3)
    axis = plt.subplot(grid[0,:-1])
    colors = colors_gen()
    plots, legend_names = list(), list()
    groups = list()
    abundance_table = list()
    width = 0.5
    for taxon, taxrow in izip(taxa, taxa_data):
        group_keys, abds = zip(*list(group_by_meta(metadata, taxrow)))
        groups = group_keys
        abundance_table.append(abds)

    abundance_table = np.array(abundance_table)
    abundance_table /= np.sum(abundance_table, axis=0)
    bins = np.arange(len(groups))

    prev = []
    for taxon, abds in zip(taxa, abundance_table):
        if not len(prev):
            plots.append(
                axis.bar(bins, abds, width,
                        color=colors.next())
            )
        else:
            plots.append(
                axis.bar(bins, abds, width,
                        color=colors.next(), bottom=prev)
            )
        legend_names.append(title_text(taxon))
        prev = abds
        
    axis.set_xticks(bins+width/2., groups)
    axis.set_yticks([])
    axis.legend(plots, legend_names, bbox_to_anchor=(1.05, 1), 
                loc=2, borderaxespad=0)
    

def summarize(taxa, taxa_data, level):
    rows = sorted([(tophylum(name), row) for name, row in zip(taxa, taxa_data)],
                  key=operator.itemgetter(0))

    groups = iter( 
        (name, np.sum(zip(*list(datum))[1], axis=0))
        for name, datum in groupby(rows, key=operator.itemgetter(0))
    )

    return zip(*groups)


def main():
    try:
        meta_fname, taxa_fname, metakey, plot_fname = sys.argv[1:]
    except Exception as e:
        print_usage(e)
        sys.exit(1)

    metadata = pie.get_metadata(meta_fname)
    taxa, taxa_data = linregress.load_otu_table(taxa_fname)
    taxa, taxa_data = summarize(taxa, taxa_data, level=2)
    taxa_data = np.array(taxa_data)

    coltotal = taxa_data.sum(axis=0)
    filter_ = ~(np.isnan(coltotal) | coltotal < 1)
    taxa_data = taxa_data.astype('float')[:,filter_] / coltotal[filter_]

    plot_stacked_bar(metadata, taxa, taxa_data)
    plt.savefig(plot_fname, dpi=500)


if __name__ == '__main__':
    ret = main()
    sys.exit(ret)
