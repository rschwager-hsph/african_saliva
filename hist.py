#!/usr/bin/env python

import sys
import traceback

import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import norm

import linregress


def print_usage(e=None):
    if e:
        print >> sys.stderr, traceback.format_exc()
    print >> sys.stderr, \
        "%s: map.txt, otu_table.txt plot_name.png" \
        %(sys.argv[0])

def plot_hist(data):
    for row in data:
        row = row[ ~np.isinf(row) ]
        heights, bins = np.histogram(row, len(row)/5)
        bins = bins[:-1]
        model = norm(row.mean(), row.std())
        plt.plot(bins, heights, 'b_')
        plt.plot(bins, model.pdf(bins)*len(row), 'r,')

    plt.ylabel("Frequency")
    plt.xlabel("Microbial Counts")
    

def main():
    try:
        meta_fname, otu_fname, plot_fname = sys.argv[1:]
    except Exception as e:
        print_usage(e)
        sys.exit(1)

    taxa, taxa_data = linregress.load_otu_table(otu_fname)

    taxa_data = np.log(taxa_data)
    plot_hist(taxa_data)
    plt.savefig(plot_fname, dpi=500)

if __name__ == '__main__':
    ret = main()
    sys.exit(ret)
